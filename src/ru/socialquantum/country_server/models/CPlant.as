package ru.socialquantum.country_server.models
{
	import ru.socialquantum.country_server.CSettings;
	
	/**
	 * Модель растения в игре
	 * @author Maxim I. Yanovsky
	 */
	public class CPlant
	{
		//стадия роста созревшего растения
		public static var STATE_READY : int = CSettings.plantReadyState;
		
		//стадия роста свежепосаженного растения
		public static var STATE_DEFAULT : int = CSettings.plantDefaultState;
		
		private var _mId : String; //уникальный id
		private var _mState : int; //стадия роста
		private var _mX : int;
		private var _mY : int;
		private var _mName : String; //идентификатор вида (картошка, морковь)
		
		public function CPlant(data : XML = null)
		{
			if (data)
			{
				_mId = data.@id;
				_mState = data.@state;
				_mX = data.@x;
				_mY = data.@y;
				_mName = data.name();
			}
		
		}
		
		/**
		 * Созрело ли растение
		 */
		public function get ready() : Boolean
		{
			return state == STATE_READY;
		}
		
		/**
		 * Функция роста
		 */
		public function growth() : void
		{
			if (!ready)
			{
				state++;
			}
		}
		
		/**
		 * Экспорт в xml
		 */
		public function get xml() : XML
		{
			var node : XML = new XML(<{name}/>);
			node.@x = x;
			node.@y = y;
			node.@state = state;
			node.@id = id;
			return node;
		}
		
		public function get id() : String
		{
			return _mId;
		}
		
		public function set id(value:String):void 
		{
			_mId = value;
		}
		
		public function get state() : int
		{
			return _mState;
		}
		
		public function set state(value : int) : void
		{
			_mState = value;
		}
		
		public function get x() : int
		{
			return _mX;
		}
		
		public function set x(value : int) : void
		{
			_mX = value;
		}
		
		public function get y() : int
		{
			return _mY;
		}
		
		public function set y(value : int) : void
		{
			_mY = value;
		}
		
		public function get name() : String
		{
			return _mName;
		}
		
		public function set name(value:String):void 
		{
			_mName = value;
		}
		
	}

}