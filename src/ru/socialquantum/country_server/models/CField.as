package ru.socialquantum.country_server.models
{
	
	/**
	 * Модель игрового поля
	 * @author Maxim I. Yanovsky
	 */
	public class CField
	{
		private var _mX : int; //координаты левого-верхнего угла поля в клетках
		private var _mY : int;
		
		private var _mWidth : int; //ширина и высота поля в клетках
		private var _mHeight : int;
		
		private var _mPlants : Vector.<CPlant>; //все растения
		private var _mId : String; //id хозяина поля
		
		public function CField(data : XML)
		{
			_mX = data.@x;
			_mY = data.@y;
			_mWidth = data.@width;
			_mHeight = data.@height;
			_mId = data.@id;
			_mPlants = new Vector.<CPlant>;
			for each (var node : XML in data.children())
			{
				plants.push(new CPlant(node));
			}
		}
		
		/**
		 * Получение растения по его id
		 * @param	id
		 * @return
		 */
		public function getPlantById(id : String) : CPlant
		{
			for each (var p : CPlant in plants)
			{
				if (p.id == id)
					return p;
			}
			return null;
		}
		
		/**
		 * Получение растения по его координатам
		 * @param	x
		 * @param	y
		 * @return
		 */
		public function getPlant(x : int, y : int) : CPlant
		{
			for each (var p : CPlant in plants)
			{
				if (p.x == x && p.y == y)
				{
					return p;
				}
			}
			return null;
		}
		
		/**
		 * Удаление растения с поля
		 * @param	plant
		 */
		public function remove(plant : CPlant) : void
		{
			var index : int = plants.indexOf(plant);
			plants.splice(index, 1);
		}
		
		/**
		 * Добавление растения на поле
		 * @param	plant
		 */
		public function add(plant : CPlant) : void
		{
			plants.push(plant);
		}
		
		/**
		 * Перемещение растения на поле
		 * @param	plant
		 * @param	x
		 * @param	y
		 */
		public function move(plant : CPlant, x : int, y : int) : void
		{
			var p : CPlant = getPlant(x, y);
			plant.x = x;
			plant.y = y;
		}
		
		/**
		 * Повышение стадии роста всех растений на поле
		 */
		public function growth() : void
		{
			for each (var p : CPlant in plants)
			{
				p.growth();
			}
		}
		
		/**
		 * Экспорт в xml
		 */
		public function get xml() : XML
		{
			var node : XML =  <field/>;
			node.@id = id;
			node.@x = x;
			node.@y = y;
			node.@width = width;
			node.@height = height;
			for each (var p : CPlant in plants)
			{
				node.appendChild(p.xml);
			}
			return node;
		}
		
		public function get x() : int
		{
			return _mX;
		}
		
		public function get y() : int
		{
			return _mY;
		}
		
		public function get width() : int
		{
			return _mWidth;
		}
		
		public function get height() : int
		{
			return _mHeight;
		}
		
		public function get plants() : Vector.<CPlant>
		{
			return _mPlants;
		}
		
		public function get id() : String
		{
			return _mId;
		}
	
	}

}