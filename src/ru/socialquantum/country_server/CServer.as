package ru.socialquantum.country_server
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.ServerSocketConnectEvent;
	import flash.net.ServerSocket;
	import flash.net.Socket;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	/**
	 * Точка входа в приложение
	 */
	public class CServer extends Sprite
	{
		private var _mServerSocket : ServerSocket = new ServerSocket();
		private var _mController : CController;
		private var _mTf : TextField = new TextField();
		;
		
		public function CServer()
		{
			if (stage)
			{
				onAddedToStage();
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(e : Event = null) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initConsole();
			CSettings.init();
			//прогоняем тесты, если все хорошо - запускаемся
			if (checkTests())
			{
				init();
			}
		}
		
		private function initConsole() : void
		{
			addChild(_mTf);
			_mTf.border = true;
			_mTf.borderColor = 0xFF0000;
			_mTf.defaultTextFormat = new TextFormat("Courier", 10, 0xFFFFFF);
			_mTf.text = "system log:\n";
			_mTf.width = stage.stageWidth;
			_mTf.height = stage.stageHeight;
			log("hello, world");
		}
		
		private function init() : void
		{
			_mController = new CController();
			_mServerSocket.bind(8080, "127.0.0.1");
			_mServerSocket.addEventListener(ServerSocketConnectEvent.CONNECT, onConnect);
			_mServerSocket.listen();
			log("Bound to: " + _mServerSocket.localAddress + ":" + _mServerSocket.localPort);
		}
		
		private function onConnect(event : ServerSocketConnectEvent) : void
		{
			event.socket.addEventListener(ProgressEvent.SOCKET_DATA, onClientSocketData);
			log("Connection from " + event.socket.remoteAddress + ":" + event.socket.remotePort);
		}
		
		private function onClientSocketData(event : ProgressEvent) : void
		{
			var buffer : ByteArray = new ByteArray();
			(event.currentTarget as Socket).readBytes(buffer, 0, (event.currentTarget as Socket).bytesAvailable);
			log("Received: " + buffer.toString());
			var socket : Socket = event.currentTarget as Socket;
			var arr : Array = buffer.toString().split("$");
			for each (var str : String in arr)
			{
				if (str != "")
				{
					doRequest(JSON.parse(str), socket);
				}
			}
			socket.flush();
			log("Sent message to " + socket.remoteAddress + ":" + socket.remotePort);
		
		}
		
		private function doRequest(request : Object, socket : Socket) : void
		{
			//отправляем полученный запрос от игрока в контроллер
			var response : XML = _mController.call(request);
			
			//это чтобы игрок смог сопоставить запрос и ответ
			response.@timestamp = request.timestamp;
			
			//отвечаем игроку
			socket.writeUTFBytes(response.toXMLString() + "$");
			log(response.toXMLString());
		}
		
		private function checkTests() : Boolean
		{
			try
			{
				new CTest();
			}
			catch (e : Error)
			{
				log("Tests failed:", e.message, e.getStackTrace());
				if (e.errorID == 3001)
				{
					log("Запустите приложение с правами администратора");
				}
				return false;
			}
			log("Tests completed");
			return true;
		}
		
		private function log(... args : Array) : void
		{
			_mTf.appendText(args.join(' ') + "\n");
			_mTf.scrollV = _mTf.maxScrollV;
		}
	}
}