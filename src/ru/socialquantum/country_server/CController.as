package ru.socialquantum.country_server
{
	import ru.socialquantum.country_server.databases.CDb;
	import ru.socialquantum.country_server.databases.IDataBase;
	import ru.socialquantum.country_server.models.CField;
	import ru.socialquantum.country_server.models.CPlant;
	import ru.socialquantum.country_server.utils.CUIDUtil;
	
	/**
	 * Основной контроллер игрового API
	 * @author Maxim I. Yanovsky
	 */
	public class CController
	{
		private var _mDb : IDataBase;
		
		public function CController()
		{
			_mDb = new CDb();
		}
		
		/**
		 * Основной шлюз.
		 * Формат запроса {method:"methodName", params:[arg1, arg2, ...]}
		 * @param	request
		 * @return
		 */
		public function call(request : Object) : XML
		{
			var obj : Object = this;
			try
			{
				var method : Function = obj[request.method];
				return method.apply(this, request.params);
			}
			catch (e : ArgumentError)
			{
				return error("wrong arguments");
			}
			catch (e : Error)
			{
				return error("unknown method: " + request.method);
			}
			catch (obj : Object)
			{
				return error(String(obj));
			}
			return null;
		}
		
		/**
		 * Получение настроек игры которые лежат в файле config.xml
		 * @param	field_id
		 * @return
		 */
		public function settingsGet(field_id : String) : XML
		{
			return CSettings.data;
		}
		
		/**
		 * Получение поля игрока
		 * @param	field_id
		 * @return
		 */
		public function fieldGet(field_id : String) : XML
		{
			var xml : XML = _mDb.getDocument(field_id)
			return xml.field[0];
		}
		
		/**
		 * Выращивание всех растений на одну стадию роста.
		 * @param	field_id
		 * @return
		 */
		public function fieldGrowth(field_id : String) : XML
		{
			var field : CField = new CField(fieldGet(field_id));
			field.growth();
			save(field);
			return field.xml;
		}
		
		/**
		 * Посадка растения plant_name на поле field_id в ячейку с координатами (x,y)
		 * @param	field_id
		 * @param	plant_name
		 * @param	x
		 * @param	y
		 * @return
		 */
		public function plantAdd(field_id : String, plant_name : String, x : int, y : int) : XML
		{
			var field : CField = new CField(fieldGet(field_id));
			
			if (!CSettings.availiblePlants.indexOf(plant_name) == -1)
				return error("wrong plant name: " + plant_name);
			
			if (field.getPlant(x, y))
				return error("wrong plant position: busy");
			
			var p : CPlant = new CPlant();
			p.name = plant_name;
			p.x = x;
			p.y = y;
			p.state = CPlant.STATE_DEFAULT;
			p.id = CUIDUtil.createUID();
			
			if (p.x >= field.x && p.x < field.x + field.width && p.y >= field.y && p.y < field.y + field.height)
			{
				field.add(p)
				save(field)
				return field.xml;
			}
			else
				return error("wrong plant position: outside field");
		}
		
		/**
		 * Метод перемещения посадки
		 * @param	field_id
		 * @param	plant_id
		 * @param	x
		 * @param	y
		 * @return
		 */
		public function plantMove(field_id : String, plant_id : String, x : int, y : int) : XML
		{
			var field : CField = new CField(fieldGet(field_id));
			var p : CPlant = field.getPlantById(plant_id);
			if (p == null)
				return error("plant with id " + plant_id + " not exists");
			
			if (field.getPlant(x, y))
				return error("wrong plant position: busy");
			
			if (x >= field.x && x < field.x + field.width && y >= field.y && y < field.y + field.height)
			{
				field.move(p, x, y);
				save(field)
				return field.xml;
			}
			else
				return error("wrong plant position: outside field");
		}
		
		/**
		 * Метод удаления посадки
		 * @param	field_id
		 * @param	plant_id
		 * @return
		 */
		public function plantRemove(field_id : String, plant_id : String) : XML
		{
			var field : CField = new CField(fieldGet(field_id));
			var p : CPlant = field.getPlantById(plant_id);
			if (p == null)
				return error("plant with id " + plant_id + " not exists");
			field.remove(p);
			save(field);
			return field.xml;
		}
		
		private function save(field : CField) : void
		{
			_mDb.saveDocument(field.id,  <country>{field.xml}</country>);
		}
		
		private function error(msg : String) : XML
		{
			return  <error>{msg}</error>
		}
	}

}