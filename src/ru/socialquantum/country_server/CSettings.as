package ru.socialquantum.country_server 
{
	import air.update.utils.FileUtils;
	import flash.errors.IllegalOperationError;
	import flash.filesystem.File;
	/**
	 * Настройки игрового процесса
	 * @author Maxim I. Yanovsky
	 */
	public class CSettings
	{
		static private var _inited : Boolean = false;
		static private var _mAvailiblePlants : Vector.<String>;
		static private var _mPlantReadyState : int;
		static private var _mPlantDefaultState : int;
		static private var _mData : XML;
		
		public function CSettings()
		{
		
		}
		
		public static function init() : void
		{
			_mData = FileUtils.readXMLFromFile(new File(File.applicationDirectory.nativePath + "/config.xml"));
			
			if (_inited) throw new IllegalOperationError("Settings already inited");
			
			_inited = true;
			
			_mPlantReadyState = int(_mData.plant.ready_state);
			_mPlantDefaultState = int(_mData.plant.default_state);
			
			_mAvailiblePlants = new Vector.<String>();
			for each (var plantName : String in _mData.plant.availible..name)
			{
				_mAvailiblePlants.push(plantName);
			}
		
		}
		
		/**
		 * Список растений доступных к посадке
		 */
		public static function get availiblePlants() : Vector.<String>
		{
			return _mAvailiblePlants;
		}
		
		/**
		 * Цифра состояния созревшего растения
		 */
		static public function get plantReadyState() : int
		{
			return _mPlantReadyState;
		}
		
		/**
		 * Цифра состояния свежепосаженного растения
		 */
		static public function get plantDefaultState() : int
		{
			return _mPlantDefaultState;
		}
		
		/**
		 * Собственно, сырая XML с настройками
		 */
		static public function get data():XML 
		{
			return _mData;
		}
	}
}