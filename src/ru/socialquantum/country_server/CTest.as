package ru.socialquantum.country_server
{
	import ru.socialquantum.country_server.models.CField;
	import ru.socialquantum.country_server.models.CPlant;
	
	/**
	 * Класс с тестами API
	 * @author Maxim I. Yanovsky
	 */
	public class CTest
	{
		private var _mController : CController;
		private var _mField : CField;
		
		public function CTest()
		{
			var i : int;
			var x : int;
			var y : int;
			var plant : CPlant;
			
			_mController = new CController();
			_mField = new CField(_mController.fieldGet("test_id"));
			
			removeAll();
			refresh();
			//засадим все поле картошкой
			//если не засадилось - беда
			plantAllWith("potato");
			refresh();
			if (_mField.plants.length != _mField.width * _mField.height)
				throw new Error("planting error");
			
			//попытаемся собрать растения (по идее у нас нельзя убирать растения которые не выросли) 
			//если получилось - беда
			removeAll();
			refresh();
			if (_mField.plants.length != 0)
				throw new Error("removing error");
			
			//попытаемся все взрастить до конца и собрать. должно получиться
			for (i = 0; i < 5; i++)
			{
				_mField.growth();
			}
			refresh();
			if (_mField.plants.length != 0)
				throw new Error("growing error");
			
			//попытаемся посадить картошку вне поля, получиться не должно
			_mController.plantAdd(_mField.id, "potato", -1, 0);
			_mController.plantAdd(_mField.id, "potato", _mField.width, 0);
			_mController.plantAdd(_mField.id, "potato", 0, -1);
			_mController.plantAdd(_mField.id, "potato", 0, _mField.height);
			refresh();
			if (_mField.plants.length != 0)
				throw new Error("planted outside");
			
			//посадим один куст и поперемещаем его по всем клеткам по диагонали, должно получиться
			_mController.plantAdd(_mField.id, "potato", 0, 0);
			refresh();
			for (x = 0, y = 0; x < _mField.width - 1, y < _mField.height - 1; x++, y++)
			{
				_mController.plantMove(_mField.id, _mField.getPlant(x, y).id, x + 1, y + 1);
				refresh();
			}
			plant = _mField.getPlant(_mField.width - 1, _mField.height - 1);
			if (_mField.plants.length != 1 || plant == null)
				throw new Error("moving error");
			
			//теперь попробуем поперемещать этот куст вне поля, получится не должно
			_mController.plantMove(_mField.id, plant.id, -1, 0);
			_mController.plantMove(_mField.id, plant.id, _mField.width, 0);
			_mController.plantMove(_mField.id, plant.id, 0, -1);
			_mController.plantMove(_mField.id, plant.id, 0, _mField.height);
			refresh();
			plant = _mField.getPlant(_mField.width - 1, _mField.height - 1);
			if (_mField.plants.length != 1 || plant == null)
				throw new Error("moved outside");
			
			//в заключение попробуем посадить куст на место другого куста
			_mController.plantAdd(_mField.id, "clover", _mField.width - 1, _mField.height - 1);
			refresh();
			plant = _mField.getPlant(_mField.width - 1, _mField.height - 1);
			if (plant.name == "clover")
				throw new Error("planted on other");
			
			//ну и переместить один на другой
			_mController.plantAdd(_mField.id, "clover", 0, 0);
			_mController.plantMove(_mField.id, plant.id, 0, 0);
			refresh();
			plant = _mField.getPlant(0, 0);
			if (plant.name != "clover")
				throw new Error("moved on other");
		}
		
		private function refresh() : void
		{
			_mField = new CField(_mController.fieldGet(_mField.id));
		}
		
		private function plantAllWith(plant_id : String) : void
		{
			for (var x : int = _mField.x; x < _mField.x + _mField.width; x++)
			{
				for (var y : int = _mField.y; y < _mField.y + _mField.height; y++)
				{
					if (_mField.getPlant(x, y) == null)
					{
						_mController.plantAdd(_mField.id, plant_id, x, y);
					}
				}
			}
		}
		
		private function removeAll() : void
		{
			for (var x : int = _mField.x; x < _mField.x + _mField.width; x++)
			{
				for (var y : int = _mField.y; y < _mField.y + _mField.height; y++)
				{
					var plant : CPlant = _mField.getPlant(x, y);
					if (plant)
						_mController.plantRemove(_mField.id, plant.id);
				}
			}
		}
	
	}

}