package ru.socialquantum.country_server.databases
{
	
	/**
	 * Интерфейс классов работающих с базой данных
	 * @author Maxim I. Yanovsky
	 */
	
	public interface IDataBase
	{
		
		function getDocument(id : String) : XML;
		
		function saveDocument(id : String, xml : XML) : void;
	
	}

}