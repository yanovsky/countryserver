package ru.socialquantum.country_server.databases
{
	import air.update.utils.FileUtils;
	import flash.errors.IllegalOperationError;
	import flash.filesystem.File;
	
	/**
	 * Реализация базы данных на файлах
	 * @author Maxim I. Yanovsky
	 */
	public class CDb implements IDataBase
	{
		
		public function CDb()
		{
			new File(File.applicationDirectory.nativePath + "/fields").createDirectory();
		}
		
		/**
		 * Метод получения документа по id игрока
		 * @param	id
		 * @return
		 */
		public function getDocument(id : String) : XML
		{
			var file : File = new File(File.applicationDirectory.nativePath + "/fields/" + id);
			if (!file.exists)
			{
				var default_field : XML = FileUtils.readXMLFromFile(new File(File.applicationDirectory.nativePath + "/default_field.xml"));
				default_field.field.@id = id;
				FileUtils.saveXMLToFile(default_field, file);
				return default_field;
			}
			return FileUtils.readXMLFromFile(file);
		}
		
		/**
		 * Сохранение документа
		 * @param	id
		 * @param	xml
		 */
		public function saveDocument(id : String, xml : XML) : void
		{
			FileUtils.saveXMLToFile(xml, new File(File.applicationDirectory.nativePath + "/fields/" + id));
		}
	
	}

}